<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class SystemController extends Controller
{
    public function index()
    {
        if (Auth::guest()) {
            return redirect('login');
        }
        if (Auth::user()) {
            return view('home');
        }
    }
}
