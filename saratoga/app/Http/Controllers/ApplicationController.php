<?php

namespace App\Http\Controllers;

class ApplicationController extends Controller
{
    public function commander()
    {
        return view('home', ['commander' => 'Martin Jedlicka (Captain)']);
    }
}
